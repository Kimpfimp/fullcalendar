from django import forms
from django.forms import ModelForm
from django.forms.widgets import TextInput, DateInput
from flatpickr import DatePickerInput, TimePickerInput
from .models import Event

class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = '__all__'
        widgets = {
                'event_name': TextInput(attrs={'placeholder': 'Name of event'}),
                'start_date': DatePickerInput(attrs={'value':'today'}, options={'dateFormat':'Y-m-d'}),
                'start_time': TimePickerInput(attrs={'value':'today'}, options={'dateFormat':'H:i'}),
                'end_date': DatePickerInput(attrs={'value':'today'}, options={'dateFormat':'Y-m-d'}),
                'end_time': TimePickerInput(attrs={'value':'today'}, options={'dateFormat':'H:i'}),
                'color': TextInput(attrs={'type': 'color'}),
                }

class AjaxEventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = {'start_date', 'start_time', 'end_date', 'end_time', 'all_day'}