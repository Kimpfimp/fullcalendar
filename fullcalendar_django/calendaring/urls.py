from django.urls import path
from . import views

urlpatterns = [
    path('calendar', views.event, name='event'),
    path('calendar/edit/<pk>', views.user_event_edit, name='user_event_edit'),
    path('calendar/update/<pk>', views.ajax_event_update, name='ajax_event_update'),
    path('calendar/add', views.add, name='event_add'),
    
]