from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic.edit import UpdateView
from django.utils import dateparse
from .forms import EventForm, AjaxEventForm
from .models import Event
import datetime
import json
import logging
from django.contrib import messages

#logging
logging.basicConfig(filename='mylog.log', level=logging.DEBUG)

# Create your views here.
"""
def event(request):
    events = Event.objects.all()
    context = {
        "events":events,
    }
    return render(request,'calendar/index.html', context)
"""

def event(request):
    all_events = Event.objects.all()

    # if filters applied then get parameter and filter based on condition else return object
    if request.GET:
        for i in all_events:
            event_sub_arr = {}
            event_sub_arr['id'] = i.event_id
            event_sub_arr['title'] = i.event_name
            start_date = datetime.datetime.strptime(str(i.start_date.datetime()), "%Y-%b-%d").strftime("%Y-%b-%d")
            start_time = datetime.datetime.strpdate(str(i.start_time.datetime()), "%H-%M-%S").strfdate("%H-%M-%S")
            end_date = datetime.datetime.strptime(str(i.end_date.datetime()), "%Y-%b-%d").strftime("%Y-%b-%d")
            end_time = datetime.datetime.strpdate(str(i.end_time.datetime()), "%H-%M-%S").strfdate("%H-%M-%S")
            event_sub_arr['start'] = start_date + "T" + start_time
            event_sub_arr['end'] = end_date + "T" + end_time
            event_arr.append(event_sub_arr)
        return HttpResponse(json.dumps(event_arr))


    context = {
        "events":all_events,
    }
    return render(request,'calendar/index.html',context)


def add(request):
    form = EventForm(request.POST or None)
    logging.debug('request.method=POST')
    logging.debug('form=%s', form)
    if form.is_valid():
        logging.debug('form is valid')
        form.save()
        logging.debug('called form.save(), result=%s', form)
        messages.success(request,"You successfully updated the event!")
        context={'form': form}
        return HttpResponseRedirect('../calendar')
    else:
        form = EventForm()
        logging.debug('form is not valid')
    return render(request, 'calendar/event_add.html', {'form': form})

def user_event_edit(request, pk=None):
    obj = get_object_or_404(Event, pk=pk)
    form = EventForm(request.POST or None, instance=obj)
    if request.method == 'POST':
        logging.debug('request.method=POST')
        logging.debug('form=%s', form)
        if form.is_valid():
           logging.debug('form is valid')
           form.save()
           logging.debug('called form.save(), result=%s', form)
           return HttpResponseRedirect('../../calendar')
        else:
            logging.debug('form is not valid')
            return HttpResponse("Form is not valid")
    else:
        logging.debug('request.method is not POST')
    return render(request, 'calendar/user_event_edit.html', {'form': form})

def ajax_event_update(request, pk=None):
    obj = get_object_or_404(Event, pk=pk)
    form = AjaxEventForm(request.POST or None, instance=obj)
    if request.method == 'POST':
        logging.debug('request.method=POST')
        logging.debug('form=%s', form)
        if form.is_valid():
           logging.debug('form is valid')
           form.save()
           logging.debug('called form.save(), result=%s', form)
           return HttpResponseRedirect('../../calendar')
        else:
            logging.debug('form is not valid')
            return HttpResponse("Form is not valid")
    else:
        logging.debug('request.method is not POST')
    return render(request, 'calendar/ajax_event_edit.html', {'form': form})