 function datetimeconvert(startdate, enddate, pk, allDay){
 // Now we can access our time at date[1], and monthdayyear @ date[0]
 var starttime = startdate[1];
 var startmdy = startdate[0];
 var endtime = enddate[1];
 var endmdy = enddate[0];
 
 // We then parse  the mdy into parts
 startmdy = startmdy.split('/');
 endmdy = endmdy.split('/');
 var startmonth = parseInt(startmdy[0]);
 var endmonth = parseInt(endmdy[0]);
 var startday = parseInt(startmdy[1]);
 var endday = parseInt(endmdy[1]);
 var startyear = parseInt(startmdy[2]);
 var endyear = parseInt(endmdy[2]);
 
 // Putting it all together
 var startformattedDate = startyear + '-' + startmonth + '-' + startday;
 var endformattedDate = endyear + '-' + endmonth + '-' + endday;


              $.ajax({
               headers : { 'X-CSRFToken': token },
               type: 'post',
               url: " calendar/update/aaa".replace('aaa', pk),
               data: {
                 'start_date': startformattedDate,
                 'start_time': starttime,
                 'end_date': endformattedDate,
                 'end_time': endtime,
                 'all_day': allDay,
               },
               success: function(response){
                 console.log(response);
             },
               error: function (info) {
                 alert("Something went wrong, check with your local nerd.");
               },
           });
        }