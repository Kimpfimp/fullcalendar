import Vue from 'vue';
import Argon from '@/plugins/argon-kit'
import flatPicker from "vue-flatpickr-component";
import "flatpickr/dist/flatpickr.css";

Vue.use(Argon);

export default {
  components: {flatPicker}
}